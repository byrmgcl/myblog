from django.conf.urls import patterns, url
from blog import views
urlpatterns = patterns(
    '',
    url(r'^$', views.PostListView.as_view(), name='index'),
    url(r'^(?P<slug>[\w-]+)/$', views.PostDetailView.as_view(), name='post_detail'),
    url(r'^category/(?P<category_slug>[\w-]+)/$',
        views.CategoryView.as_view(), name='category'),
    url(r'^tag/(?P<tag_slug>[\w-]+)/$', views.TagView.as_view(), name='tag'),
    url(r'^archive/(?P<year>\d{4})/(?P<month>\w+)/$',
        views.ArchiveView.as_view(), name='archive'),
)
