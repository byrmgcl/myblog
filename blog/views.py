from django.shortcuts import render, get_object_or_404
from .models import Post, Category, Tag, Link
from django.views.generic import (
    DetailView, MonthArchiveView, ArchiveIndexView
)
from django.utils import timezone


class PostListView(ArchiveIndexView):
    model = Post
    template_name = 'blog/post_list.djhtml'
    paginate_by = 7
    date_field = 'published_date'
    queryset = Post.objects.pub_posts()


class PostDetailView(DetailView):
    model = Post
    template_name = 'blog/post_detail.djhtml'
    queryset = Post.objects.pub_posts()


class CategoryView(PostListView):
    def get_queryset(self):
        posts = super(CategoryView, self).get_queryset()
        category = get_object_or_404(Category,
                                     slug=self.kwargs.get('category_slug'))
        return posts.filter(category=category)


class TagView(PostListView):
    def get_queryset(self):
        posts = super(TagView, self).get_queryset()
        tag = get_object_or_404(Tag,
                                slug=self.kwargs.get('tag_slug'))
        return posts.filter(tags=tag)


class ArchiveView(MonthArchiveView):
    model = Post
    date_field = 'published_date'
    template_name = 'blog/post_list.djhtml'
