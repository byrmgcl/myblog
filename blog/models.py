from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.template.defaultfilters import slugify
from django.utils import timezone


class PostQuerySet(models.QuerySet):
    def pub_posts(self):
        return self.filter(published_date__isnull=False)


class PostManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)

    def pub_posts(self):
        return self.get_queryset().pub_posts()


class Category(models.Model):
    name = models.CharField(max_length=128, unique=True)
    slug = models.SlugField(max_length=255, unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'category'
        verbose_name_plural = 'categories'


class Tag(models.Model):
    name = models.CharField(max_length=55, unique=True)
    slug = models.SlugField(max_length=77, unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Tag, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Post(models.Model):
    author = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    tags = models.ManyToManyField(Tag, blank=True)
    title = models.CharField(max_length=255)
    content = models.TextField()
    slug = models.SlugField(max_length=300, unique=True)
    created_date = models.DateTimeField()
    published_date = models.DateTimeField(blank=True, null=True)
    modified_date = models.DateTimeField(auto_now_add=True)
    objects = PostManager()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        if not self.created_date:
            self.created_date = timezone.now()
        super(Post, self).save(*args, **kwargs)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('index')

    class Meta:
        ordering = ('-published_date',)


class Link(models.Model):
    name = models.CharField(max_length=155)
    url = models.URLField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class How(models.Model):
    question = models.CharField(max_length=255)
    answer = models.TextField()
    slug = models.SlugField(max_length=300, unique=True)

    class Meta:
        ordering = ('question',)

    def __str__(self):
        return self.question

    def save(self, *args, **kwargs):
        self.slug = slugify(self.question)
        super(How, self).save(*args, **kwargs)
