# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_auto_20150513_1638'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='link',
            options={'ordering': ('name',)},
        ),
        migrations.AlterField(
            model_name='post',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 13, 13, 50, 46, 62983, tzinfo=utc)),
        ),
    ]
