# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20150513_1650'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='post',
            options={'ordering': ('published_date',)},
        ),
        migrations.AlterField(
            model_name='post',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 13, 20, 56, 13, 584260, tzinfo=utc)),
        ),
    ]
