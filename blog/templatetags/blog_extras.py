from django import template
from blog.models import (
    Category, Tag, Link, Post
)

register = template.Library()


@register.inclusion_tag('blog/categories.djhtml')
def get_category_list():
    return {'categories': Category.objects.all()}


@register.inclusion_tag('blog/tags.djhtml')
def get_tag_list():
    return {'tags': Tag.objects.all()}


@register.inclusion_tag('blog/links.djhtml')
def get_link_list():
    return {'links': Link.objects.all()}


@register.inclusion_tag('blog/recent_posts.djhtml')
def get_recent_post_list():
    return {'recent_posts': Post.objects.all()[:10]}


@register.inclusion_tag('blog/archives.djhtml')
def get_archive_list():
    pub_dates = (post.published_date for post in Post.objects.all())
    months = []
    for pub_date in pub_dates:
        year = pub_date.strftime('%Y')
        s_month = pub_date.strftime('%b')
        l_month = pub_date.strftime('%B')
        if (year, s_month, l_month) not in months:
            months.append((year,
                           s_month,
                           l_month))
    return {'months': months}
