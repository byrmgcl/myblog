import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'weblog.settings')

import django
django.setup()

from blog.models import Category, Post, Tag
from django.contrib.auth.models import User
from django.contrib.auth import authenticate


def get_or_create_author():
    try:
        author = User.objects.get(username='test')
    except User.DoesNotExist:
        author = User.objects.create_superuser(username='test', email='byrmgcl@yandex.com.tr', password='123')
    return author

def add_cat(name):
    create_category = Category.objects.get_or_create(name=name)[0]
    return create_category

def add_post(author, cat, title, content, tag=None):
    create_post = Post.objects.get_or_create(
        author=author, category=cat,
        title=title, content=content
    )[0]
    if tag:
        create_post.tags.add(tag)
    return create_post

def parse():
    source = {}
    for i in os.listdir('wordpress'):
        if '--' in i:
            names = os.path.splitext(i)[0].split('--')[1:]

        if len(names) == 1:
            continue

        category = names[0].replace('_', ' ')
        title = names[1].replace('_', ' ')

        source.setdefault(category, {})
        source[category].setdefault(title)

        file = open(os.path.join('wordpress', i))
        contents = file.readlines()

        if contents[1] != '\n':
            content = ''.join(contents[2:])
        else:
            content = ''.join(contents[1:])

        source[category][title] = content
    return source

def populate():
    author = get_or_create_author()
    for cat in parse():
        category = add_cat(cat)
        for title in parse()[cat]:
            content = parse()[cat][title]
            add_post(author, category, title, content)

def publish():
    posts = Post.objects.all()
    for i in posts:
        i.publish()

if __name__ == '__main__':
    populate()
    publish()
