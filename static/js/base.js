var $jq = jQuery.noConflict();

(function () {
    $jq('.default-border').mouseover(function (){
	$jq(this).css('border-color', '#D60052');
    })
	.mouseout(function (){
	    $jq(this).css('border-color', '#1E90FF');
	});

    $jq('#slogan a').mouseover(function (){
	$jq('#slogan-line').css('border-color', '#D60052');
    })
	.mouseout(function (){
	    $jq('#slogan-line').css('border-color', '#1E90FF');
	});

})();
